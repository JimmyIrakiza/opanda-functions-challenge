package opanda;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.regex.*;
import java.util.stream.Stream;

public class Function {
	
	public void findEmailsInAString(String text) {
		int textLength = text.length();
		if(text.isBlank()|| text.isEmpty() || text.length()<=0) {
			System.out.println("The String is empty!");	
		}else {
			//Regex for email
			String emailFormat = "^(.+)@(.+)$";
			//Creating array of strings in a text
			String[] words = text.split(" ");
			List<String> emails = new ArrayList<String>();
			for(int i=0; i<words.length; i++) {
				//if a string in array matches the regex, then it is added in returned array
				if(words[i].matches(emailFormat)) {
					emails.add(words[i].toUpperCase());
				}
			}
			
			if(emails.size()>0) {
				System.out.println(emails.size()+ " Emails found");
				for(String e:emails) {
					System.out.println(e);
				}
			}else {
				System.out.println("No Emails found");
			}
		}
		
	}
	
	public void findDateAndTimeStamp(String text) {
		int textLength = text.length();
		//Regex for dates and time stamp
		String dateFormat = "[0-9]{1,4}[-|/]{1}[0-9]{1,2}[-|/]{1}[0-9]{1,2}";
		String timeFormat = "[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}";
		//Creating array of strings in a text
		if(text.isBlank()|| text.isEmpty() ||text.length()<=0) {
			System.out.println("The String is empty!");
		}else {
			String[] dates = text.split(" ");
			List<String> dateList = new ArrayList<String>();
			for(int i=0; i<dates.length; i++) {
				//if a string in array matches the regex, then it is added in displayed array
				if(dates[i].matches(dateFormat)) {
					dateList.add(dates[i]); 
				}else if(dates[i].matches(timeFormat)) {
					dateList.add(dates[i]);
				}
			}
			if(dateList.size()>0) {
				System.out.println(" Dates & Time Stamp found");
				for(String e:dateList) {
					System.out.println(e);
				}
			}else {
				System.out.println("No Dates & Time Stamp found");
			}
		}
		
	}
	
	public ArrayList<String> wordArrangements(String word) {
	    ArrayList<String> possibleArrangements = new ArrayList<String>();
	    if (word.length() == 1) {
	    	possibleArrangements.add(word);
	    } else if (word.length() > 1) {
	        int lastIndex = word.length() - 1;
	        String lastChar = word.substring(lastIndex);
	        String restOfString = word.substring(0, lastIndex);
	        possibleArrangements = mergeRestStringAndLastChar(wordArrangements(restOfString), lastChar);
	    }
	    return possibleArrangements;
	}

	private ArrayList<String> mergeRestStringAndLastChar(ArrayList<String> restStr, String lastChar) {
	    ArrayList<String> mergedArrangements = new ArrayList<>();
	    for (String string : restStr) {
	        for (int i = 0; i <= string.length(); ++i) {
	            String possiblePost = new StringBuffer(string).insert(i, lastChar).toString();
	            mergedArrangements.add(possiblePost);
	        }
	    }
	    return mergedArrangements;
	}
	
	
	public void listingImagenOtherFiles(String path) {
		Path thePath = Paths.get(path);
		try(Stream<Path> content = Files.walk(thePath, 1)) {
			content.filter(Files::isRegularFile).forEach(System.out::println);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void averageOfNumbers(String text) {
		int textLength = text.length();
		String number  = "";
		int sumofDigits = 0, numberOfDigits = 0;
		//loop to find each word in a text
		for(int i = 0; i<textLength; i++) {
			//testing if a character is a number
			if(Character.isDigit(text.charAt(i))) {
				if(i<textLength-1 && Character.isDigit(text.charAt(i+1))) {
					number += text.charAt(i);
				}else {
					number += text.charAt(i);
					numberOfDigits++;
					//adding numbers from string
					sumofDigits += Integer.parseInt(number);
					number ="";
				}
			}
		}
		System.out.println("The average is: "+sumofDigits/numberOfDigits);

	}
	
	public void capitalizeEachWord(String text) {
		String[] words = text.split(" ");
		for(int i=0; i<words.length; i++) {
			words[i] = words[i].substring(0,1).toUpperCase()+words[i].substring(1).toLowerCase();
		}
		for(int i=0; i<words.length; i++) {
			System.out.print(words[i]+" ");
		}
	}
	
	public void replaceHyperlinks(String text, String replacingString) {
		int counter =0;
		if(text.isBlank()|| text.isEmpty() ||text.length()<=0) {
			System.out.println("The String is empty!");
		}else {
		String[] words = text.split(" ");
		String linkRegex = "^www.[a-zA-Z0-9\\-\\.]+\\.(rw|ug|uk|com|org|net|mil|edu|COM|ORG|NET|MIL|EDU)$";
		for(int i=0; i<words.length; i++) {
			if(words[i].matches(linkRegex)) {
				words[i] = replacingString;
				counter = 1;
			}
		}
		if(counter==0) {
			System.out.println("No links found");
		}else {
			System.out.println("Text after replacing links");
			for(int i=0; i<words.length; i++) {
				System.out.print(words[i]+" ");
			}
		}
		
		}
	}
}
