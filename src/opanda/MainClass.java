package opanda;

public class MainClass {

	public static void main(String[] args) {
		String text = "I'm sending my www.google.com 2010-06-29 13:30:10 CV on www.wkila.rw irakiza@gmail.com if this email is not pleasant to you use jimmy@gmail.com or irakizajimmy1@gmail.com";

		// Function a
		System.out.println("FUNCTION a:");
		Function f9a = new Function();
		f9a.findEmailsInAString(text);

//		Funcion b
		System.out.println(" ");
		System.out.println("FUNCTION b:");
		Function f9b = new Function();
		f9b.findDateAndTimeStamp(text);

		// Function c
		System.out.println(" ");
		System.out.println("FUNCTION c:");
		Function f9c = new Function();
		for (String d : f9c.wordArrangements("JIM")) {
			System.out.print(d + " ");
		}

		// Function d
		System.out.println(" ");
		System.out.println("FUNCTION d:");
		Function f9d = new Function();
		f9d.listingImagenOtherFiles("E://FFOutput");

		// Function e
		System.out.println(" ");
		System.out.println("FUNCTION e:");
		Function f9e = new Function();
		f9e.averageOfNumbers("3 and kalisa56 jim6fi nez5j");

		// Funcion f
		System.out.println(" ");
		System.out.println("FUNCTION f:");
		Function f9f = new Function();
		f9f.capitalizeEachWord(text);

		// Function g
		System.out.println(" ");
		System.out.println("FUNCTION g:");
		Function f9g = new Function();
		f9g.replaceHyperlinks(text, "CHANGED LINK");
	}
}
